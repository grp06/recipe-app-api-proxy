# Recipe app API proxy

nginx proxy app for our recipe app API

## usage

### environment vars

- `LISTEN_PORT` - port to listen on (default: 8000)
- `APP_HOST` - Hostname of the app to forward requests to (default `app`)
- `APP_PORT` - Port of the app to forward requests to (default: `9000`)
